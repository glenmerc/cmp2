<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<div class="container">
<div style="margin-top:60px !important">
	<div class="row">
    	<div class="col-md-10 col-md-offset-1">
        	<div class="panel panel-default">
            	<?php while(have_posts()) : the_post(); ?>
        			<div class="panel-heading">
                		<h2 class="panel-title"><?php the_title(); ?></h2>
            		</div>
        			<div class="panel-body">
						<p><?php the_content(' '); ?></p> 
 				<?php endwhile;  ?>
            		</div>
            </div>
            

    	</div>
	</div>
    </h2>
</div>

<?php get_footer(); ?>
