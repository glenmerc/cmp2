<?php
/*
Template Name: Alle berichten
*/
?>
<?php get_header(); ?>

<div class="container">
	<div class="row">
    		<div style="margin-top:30px;"></div>
            <div class="col-md-12">
            	<h1>Berichten</h1>
                <hr>
            </div>
            <div class="col-md-9">
            
            <?php 
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	query_posts('posts_per_page=5&cat=-16,-24&paged=' . $paged);
		while(have_posts()) : the_post(); ?>
        	<div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                    <div class="panel-body">
                        <p><?php the_content(); ?></p><hr>
                        <p style="font-size:10px; font-style:italic !important; color:rgba(3,151,245,1.00) !important" class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('G:i, F jS Y') ?>, Category: <?php the_category(', '); ?> </p>
                    </div>
                </div>
            </div>
 <?php endwhile;  ?>

<div style="text-align:center"><?php wp_pagenavi(); ?></div>
</div>
<div class="col-md-3">
        	<div class="list-group">
        		<?php query_posts('posts_per_page=20&cat=-16,-24'); while(have_posts()) : the_post(); ?>
                	<a href="<?php the_permalink();  ?>" class="list-group-item">
                    <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
                    <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS G:i, Y') ?> </p>
                    </a>
                <?php endwhile; wp_reset_query(); ?> 
            </div>
    	</div>
        <div style="text-align:center">
        </div> 
</div>
</div>
<?php get_footer(); ?>
