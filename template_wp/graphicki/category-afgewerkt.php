<?php 
/**
 * The template for displaying all single posts and attachments
 */
 
get_header(); ?>
<div class="container">
<div style="margin-top:60px">
	<div class="row">
    	<h1>Afgewerkte Projecten</h1><hr>
    	<div class="col-md-9">
        	<div class="panel panel-default panel-body">
            	<?php while(have_posts()) : the_post(); ?>
        		<h2><a><?php the_title(); ?></a></h2>
                <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS, Y') ?> </p>
                <hr>
				<p style="font-size:24px !important"><?php the_content(' '); ?></p> 
                <?php if(function_exists('like_counter_p'))?>
 				<?php endwhile;  ?>
            </div>
    	</div>
        <div class="col-md-3">
        	<div class="list-group">
        		<?php query_posts('posts_per_page=8&cat=16'); while(have_posts()) : the_post(); ?>
                	<a href="<?php the_permalink();  ?>" class="list-group-item">
                    <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
                    <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS G:i, Y') ?> </p>
                    </a>
                <?php endwhile; wp_reset_query(); ?> 
            </div>
    	</div>
	</div>
</div>
<div class="col-md-6"><?php global $withcomments; $withcomments = true;
comments_template( '', true ); ?></div>
<?php get_footer(); ?>
