<?php get_header(); ?>
<div class="container">
<div style="margin-top:30px !important">
	<div class="row">
    	<div class="col-md-9">
        	<p style="font-size:40px; text-align:center"><?php single_cat_title(); ?></p>
            <?php while(have_posts()) : the_post(); ?>
        	<div class="panel panel-default">
        			<div class="panel-heading">
                		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            		</div>
        			<div class="panel-body">
                    
						<p><?php the_content(' '); ?></p> <hr>
                        <p style="font-size:12px; font-style:italic !important; color:rgba(3,151,245,1.00) !important" class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('G:i, F jS Y') ?> </p>
                        
 				
            		</div>
            </div>
            <?php endwhile;  ?>

    	</div>
        <div class="col-md-2">
			<?php get_sidebar(); ?>
		</div>
	</div>
    </h2>
</div>
<?php get_footer(); ?>
