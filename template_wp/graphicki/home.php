<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<div style="margin-top:-30px;"  >
<?php if ( function_exists( 'easingslider' ) ) { easingslider( 36 ); } ?>
</div>

<div class="container">
	<div class="row">
        <div>
           	<h1>Updates</h1><hr>
        </div>
     	<div class="col-md-12">
            <?php 
	query_posts('posts_per_page=3');
		while(have_posts()) : the_post(); ?>
        	<div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                    <div class="panel-body">
                        <p style=""><?php the_excerpt(); ?></p>
                        <p style="font-size:10px; font-style:italic !important; color:rgba(3,151,245,1.00) !important" class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('G:i, F jS Y') ?> / <?php the_category(', '); ?>  </p>
                        
                    </div>
                </div>
            </div>
 <?php endwhile;  ?>
 		</div>
    </div>    
 		
	<div class="col-md-12">
   		<div class="row">
        	<div>
            	<h1>Algemeen</h1><hr>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<?php
						$post_id = 200;
						$queried_post = get_post($post_id);
						$title = $queried_post->post_title;
						?>
                        <h3><i class="fa fa-wordpress" aria-hidden="true"></i>
<?php echo $title; ?></h3>
                    </div>
                    <div class="panel-body">
                        <p><?php echo $queried_post->post_content;?></p>     
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    	<?php
						$post_id = 205;
						$queried_post = get_post($post_id);
						$title = $queried_post->post_title;
						?>
                        <h3><i class="fa fa-headphones" aria-hidden="true"></i><?php echo $title; ?></h3>
                    </div>
                    <div class="panel-body">
                        <p><?php echo $queried_post->post_content;?></p>     
                    </div>
                </div>
            </div>
    	 </div>
	</div>





<?php get_footer(); ?>