<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
<div class="container">
	<div style="margin-top:60px">
		<div class="row">
    		<?php while(have_posts()) : the_post(); ?>
    	<div class="col-md-9">
        	<div class="panel panel-default panel-body">
        		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS, Y') ?> </p>
                <hr>
				<p style="font-size:24px !important"><?php the_content(' '); ?></p>  
 				<?php endwhile;  ?>
            </div>
    	</div>
        <div class="col-md-3" style="margin-top:-68px !important">
        <?php get_sidebar(); ?>
        </div>
        <div class="">
        <?php if(is_user_logged_in()) { ?>
            <section class="forum">
                <div class="col-md-9">
                    <?php the_field("forum_id"); ?>
                </div>
            </section>
            <?php } ?>
        </div>
       </div>
<?php get_footer(); ?>
