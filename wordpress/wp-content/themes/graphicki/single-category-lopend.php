<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
<div class="container">
<div style="margin-top:60px">
	<div class="row">
    	<h1>Lopende Projecten</h1><hr>
    	<div class="col-md-9">
        	<div class="panel panel-default panel-body">
            	<?php while(have_posts()) : the_post(); ?>
        		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS, Y') ?> </p>
                <hr>
				<p style="font-size:24px !important"><?php the_content(' '); ?></p>  
 				<?php endwhile;  ?>
            </div>
    	</div>
        <div class="col-md-3">
        	<div class="list-group">
        		<?php query_posts('posts_per_page=8&cat=24'); while(have_posts()) : the_post(); ?>
                	<a href="<?php the_permalink();  ?>" class="list-group-item">
                    <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
                    <p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS G:i, Y') ?> </p>
                    </a>
                <?php endwhile; wp_reset_query(); ?> 
            </div>
    	</div>
	</div>
</div>
</div>
<div class="col-md-9"><div id="vc-feelback-main" data-access-token="62d9d96861b19c92e2fcda46942bc52a" data-display-type="4" data-article-id="ARTICLE ID" ></div> </div>

<script> 
(function() { 
var v = document.createElement('script'); v.async = true; 
v.src = "http://assets-prod.vicomi.com/vicomi.js"; 
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(v, s); 
})(); 
</script>
<div class="col-md-6"><?php global $withcomments; $withcomments = true;
comments_template( '', true ); ?></div>
<?php get_footer(); ?>
