<?php
/**
 * The template for displaying portfolio posts and attachments
 */

get_header(); ?>
<div class="container">
<div style="margin-top:60px">
	<div class="row">
    	<div class="col-md-9">
        <?php while(have_posts()) : the_post(); ?>
        	<div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
      					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    </div>
                 	<div class="panel panel-body">
                		<p class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('F jS, Y') ?> </p>
                		<hr>
						<p style="font-size:24px !important"><?php the_content(' '); ?></p>  
                    </div>
            	</div>
         	</div>
         <?php endwhile;  ?>
         <div style="text-align:center"><?php wp_pagenavi(); ?></div>
    </div>
    <div class="col-md-3" style="margin-top:-68px !important"><?php get_sidebar(); ?></div>
	</div>


<?php get_footer(); ?>
