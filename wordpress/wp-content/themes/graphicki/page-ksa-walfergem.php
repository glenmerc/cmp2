<?php get_header(); ?>

<div class="container">
<div style="margin-top:60px !important">
	<div class="row">
    	<div class="col-md-12">
        	<div class="panel panel-default">
            	<?php while(have_posts()) : the_post(); ?>
        			<div class="panel-heading">
                		<h2 class="panel-title"><?php the_title(); ?></h2>
            		</div>
        			<div class="panel-body">
						<p><?php the_content(' '); ?></p> 
                        <p style="font-size:12px; font-style:italic !important; color:rgba(3,151,245,1.00) !important" class="list-group-item-text">Posted by <?php the_author(); ?> on <?php the_time('G:i, F jS Y') ?> </p>
 				<?php endwhile;  ?>
            		</div>
            </div>
            

    	</div>
	</div>
    </h2>
</div>

<?php get_footer(); ?>
