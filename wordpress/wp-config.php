<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2eo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}q-U]Wjs,D^zL.`d2B4v%:9,b_2%L^nGz6gDJ7~:H-dXs*,Nmm:K(fWS^TcHbvi(');
define('SECURE_AUTH_KEY',  '/r7c{#+!pfjF!G0gqC%JUW2,&U|S[=d;08Z+M--;)(NVy>D%Em.5Zj1F.H8m#{h+');
define('LOGGED_IN_KEY',    '`~q/+E.#99 -}0qa{tn$S+`6ZlD/>]/u%V#:S_|m./Z;&lVHXmSTcO2XHJ6R=99f');
define('NONCE_KEY',        'C!A@:p]vCf3=|4Vph2s?=oVONt-n/&5fmaT,?16*#vQ=]l+PzmeEs|mY+1kpFG`~');
define('AUTH_SALT',        'j*WN0x?)}Xy9y<X2w&gP~Bb8>m/.=zHt0g0UN%F0vn ;cXr;ym8&L.6O{B6L3D+y');
define('SECURE_AUTH_SALT', '+}f]/,s?^p4avN^A9zBh=ls5;-l5$Ep~gL?&&L#C2p*B_jvWL$J;XjL|L#{}7J~ ');
define('LOGGED_IN_SALT',   'YMF|!$=j$g*Wn+9XUS9{`NpP?lYVR/6vtuazYYV6E-My+T|Bhgs|cV;(+;C@-m(9');
define('NONCE_SALT',       '?w#tzC98}OyGE#S:k`,k!E3vt5=wXn68}=Y}uMCm;qTuZ]SRWlWmwE!tAC1?t@Y6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
